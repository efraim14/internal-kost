<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Transaksi', function (Blueprint $table) {
            $table->increments('TransaksiId');
            $table->integer('ClientId');
            $table->integer('UnitKosId');
            $table->string('TipeKos')->comment('Tipe Kos Bulanan / Harian');
            $table->date('TanggalMasuk');
            $table->date('TanggalKeluar')->nullable();
            $table->date('JatuhTempo');
            $table->string('Harga');
            $table->boolean('Parkir')->default(0);
            $table->boolean('Pembantu')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Transaksi');
    }
}
