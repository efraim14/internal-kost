<?php

use App\Settings;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = new Settings;
        $settings->Name = 'JatuhTempo';
        $settings->Label = 'Jatuh Tempo (dalam hari)';
        $settings->Value = 33;
        $settings->save();

        $settings = new Settings;
        $settings->Name = 'HargaBulanan';
        $settings->Label = 'Harga kost bulanan';
        $settings->Value = 2000000;
        $settings->save();

        $settings = new Settings;
        $settings->Name = 'HargaHarian';
        $settings->Label = 'Harga kost harian';
        $settings->Value = 100000;
        $settings->save();

        $settings = new Settings;
        $settings->Name = 'Parkir';
        $settings->Label = 'Harga Parkir';
        $settings->Value = 300000;
        $settings->save();

        $settings = new Settings;
        $settings->Name = 'Pembantu';
        $settings->Label = 'Harga Pembantu';
        $settings->Value = 100000;
        $settings->save();

        $settings = new Settings;
        $settings->Name = 'RecipientMail';
        $settings->Label = 'Penerima email setiap jatuh tempo harian';
        $settings->Value = 'imbadewarata@yahoo.com';
        $settings->save();

    }
}