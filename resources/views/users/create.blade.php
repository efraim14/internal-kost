@extends('layouts.dashboard')

@section('title')
Pengguna - Input
@endsection

@section('content')
<div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Pengguna Baru</div>
        <div class="card-body">
            @if($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            @elseif(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif

            @if($method == 'create')
            {{ Form::open(['route' => 'users.store', 'files' => true]) }}
            @else
            {{ Form::model($data, [
                'method' => 'PUT',
                'route' => ['users.update', $data->ClientId],
                'files' => true
            ]) }}
            @endif

            <div class="form-group">
                {!! Form::label('name', 'Nama', ['class' => 'control-label']) !!}
                {!! Form::text('ClientName', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('title', 'Foto - (max. size 10MB)', ['class' => 'control-label']) !!}
                <br>{!! Form::file('Foto', ['class' => 'form-control', 'accept' => 'image/jpeg, image/png, image/bmp, image/gif', 'capture' => 'camera']) !!}
                <small id="fotoDescription" class="form-text text-muted">Kosongkan foto jika tidak ada perubahan.</small>
            </div>
              {{Form::submit('Submit',['class'=>'btn btn-success form-control'])}}
            {{ Form::close() }}
        </div>
      </div>
@endsection
