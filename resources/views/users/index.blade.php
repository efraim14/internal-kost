@extends('layouts.dashboard')

@section('title')
Pengguna
@endsection

@section('content')
<div class="card mb-3">
  <div class="card-header">
    <i class="fa fa-table"></i> Data Pengguna</div>
  <div class="card-body">
  @if(Session::has('message'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
  @endif
    <div class="float-right col-md-4 mb-2">
      <a href="{{route('users.create')}}">
        <button type="button" class="btn btn-primary float-right">Add New</button>
      </a>
    </div>
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>ID Client</th>
            <th>Nama</th>
            <th>Kost</th>
            <th>Dibuat</th>
            <th>Diubah Terakhir</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</div>
@endsection


@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
    $('#dataTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "{{route('users.table')}}",
        "columns": [ 
          { data: "ClientId" },
          { data: "ClientName" },
          { data: "transaksi", "render": function ( data, type, row, meta ) {
              if(data.length > 0) {
                return 'Berlangganan'
              } else {
                return 'Tidak Aktif'
              }
            }
          },
          { data: "created_at", "render": function ( data, type, row, meta ) {
              return convertDate(data);
            }
          },
          { data: "updated_at", "render": function ( data, type, row, meta ) {
              return convertDate(data);
            }
          },
          { data: "ClientId", "render": function ( data, type, row, meta ) {
              var url = '{{route("transaksi.index", ["clientid" => ":id"])}}';
              url =  url.replace(':id', data);
              
              return '<a href="'+url+'"><button type="button" class="btn btn-info">Detail</button></a>';
            }
          },
          { data: "ClientId", "render": function ( data, type, row, meta ) {
              var url = '{{route("users.delete", ["clientid" => ":id"])}}';
              url =  url.replace(':id', data);
              
              return '<a href="'+url+'" onclick="return confirm(\'Apakah anda yakin?\')"><button type="button" class="btn btn-danger">Delete</button></a>';
            }
          }
        ]
    } );
} );
</script>
@endpush