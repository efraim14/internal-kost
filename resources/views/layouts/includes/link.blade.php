<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<!-- Bootstrap core CSS-->
<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="{{asset('datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="{{asset('css/sb-admin.css')}}" rel="stylesheet">
<link href="{{asset('css/evert.css')}}" rel="stylesheet">

<!-- Bootstrap core JavaScript-->
<script src="{{asset('jquery/jquery.min.js')}}"></script>
<script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
<!-- Core plugin JavaScript-->
<script src="{{asset('jquery-easing/jquery.easing.min.js')}}"></script>
<!-- Page level plugin JavaScript-->
<script src="{{asset('datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('datatables/dataTables.bootstrap4.js')}}"></script>
<!-- Custom scripts for all pages-->
<script src="{{asset('js/sb-admin.min.js')}}"></script>
<!-- Custom scripts for this page-->

<!-- Format DateTime -->
<script>
var months = new Array(12);
months[0] = "Jan";
months[1] = "Feb";
months[2] = "Mar";
months[3] = "Apr";
months[4] = "May";
months[5] = "Jun";
months[6] = "Jul";
months[7] = "Aug";
months[8] = "Sep";
months[9] = "Oct";
months[10] = "Nov";
months[11] = "Dec";

function convertDate(inputFormat) {
  if(!inputFormat) return '-';
  function pad(s) { return (s < 10) ? '0' + s : s; }
  var d = new Date(inputFormat);
  var date = [pad(d.getDate()), months[d.getMonth()], d.getFullYear()].join(' ');
//   var time = [pad(d.getHours()), pad(d.getMinutes()), d.getSeconds()].join(':');
  return date;
}
</script>

<!-- Format Boolean -->
<script>
function booleanAnswer(answer) {
    if (answer) {
        return "<font color='green'>Ya</font>";
    } else {
        return "<font color='red'>Tidak</font>";
    }
}
</script>

<!-- Convert Currency -->
<script>
function convertCurrency(n) {
    if(!n) return '-';
    n = parseFloat(n);
    return "Rp. " + n.toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}
</script>