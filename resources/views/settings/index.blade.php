@extends('layouts.dashboard')

@section('title')
Pengaturan
@endsection

@section('content')
<div class="card mb-3">
    <div class="card-header">
        <i class="fa fa-table"></i> Setting Aplikasi Kost</div>
    <div class="card-body">
        @if($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @elseif(Session::has('message'))
        <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
        @endif
        {{ Form::model($Settings, [
            'method' => 'PUT',
            'route' => ['settings.update']
        ]) }}
        
        @foreach($Settings as $setting)
        <div class="form-group">
                {!! Form::label('settings', $setting->Label, ['class' => 'control-label']) !!}
                {!! Form::text($setting->Name, $setting->Value, ['class' => 'form-control']) !!}
        </div>
        @endforeach

        {{Form::submit('Submit',['class'=>'btn btn-success form-control'])}}
        {{ Form::close() }}
    </div>
</div>

@endsection