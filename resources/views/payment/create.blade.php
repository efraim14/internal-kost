@extends('layouts.dashboard')

@section('title')
Transaksi - Input {{$data->ClientName}}
@endsection

@section('content')
<div class="card mb-3">
    <div class="card-header">
        <i class="fa fa-table"></i> Pembayaran Kost</div>
    <div class="card-body">
        @if($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @elseif(Session::has('message'))
        <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
        @endif
        @if($method == 'create')
            {{ Form::open(['route' => 'payment.store']) }}
            {!! Form::hidden('TransaksiId', $data->TransaksiId) !!}
            {!! Form::hidden('ClientId', $data->ClientId) !!}
        @else
            {{ Form::model($data, [
                'method' => 'PUT',
                'route' => ['payment.update', $data->PaymentId]
            ]) }}
        @endif

        <div class="form-group">
                {!! Form::label('tanggalpembayaran', 'Tanggal Pembayaran (Bulan / Tanggal / Tahun)', ['class' => 'control-label']) !!}
                {!! Form::date('TanggalPembayaran', \Carbon\Carbon::now(), ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
                {!! Form::label('jatuhtempo', 'Jatuh Tempo (Bulan / Tanggal / Tahun)', ['class' => 'control-label']) !!}
                {!! Form::date('UpdateJatuhTempo', $data->TipeKos=='Bulanan' ? \Carbon\Carbon::now()->addDays($JatuhTempo) : \Carbon\Carbon::now()->addDay(), ['class' => 'form-control', 'id' => 'JatuhTempo']) !!}
                <div class="form-check form-check-inline">
                    {!! Form::label('payment', 'Pembayaran Selesai?', ['class' => 'form-check-label', 'for' => 'pembayaran']) !!} 
                    &nbsp;<input class="form-check-input" type="checkbox" value="1" id="payment" name="FinishPayment" onclick="checkPayment()">
                </div>
        </div>
        <div class="form-group">
            {!! Form::label('jumlah', 'Jumlah', ['class' => 'control-label']) !!}
            {!! Form::number('Jumlah', null, ['class' => 'form-control', 'id' => 'Jumlah']) !!}
        </div>
        {{Form::submit('Submit',['class'=>'btn btn-success form-control'])}}
        {{ Form::close() }}
    </div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">
function checkPayment() {
    if (document.getElementById('payment').checked) {
        document.getElementById('JatuhTempo').value = '2999-12-31';
        document.getElementById('JatuhTempo').readOnly = true;
    } else {
        document.getElementById('JatuhTempo').readOnly = false;
        document.getElementById('JatuhTempo').value = '{!! $data->TipeKos=="Bulanan" ? \Carbon\Carbon::now()->addDays($JatuhTempo)->format("Y-m-d") : \Carbon\Carbon::now()->addDay()->format("Y-m-d") !!}';
    }
}
</script>
@endpush