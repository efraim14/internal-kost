@extends('layouts.dashboard')

@section('title')
Transaksi - Input {{$data->ClientName}}
@endsection

@section('content')
<div class="card mb-3">
    <div class="card-header">
        <i class="fa fa-table"></i> Masa Kost Baru</div>
    <div class="card-body">
        @if($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @elseif(Session::has('message'))
        <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
        @endif
        @if($method == 'create')
            {{ Form::open(['route' => 'transaksi.store']) }}
            {!! Form::hidden('ClientId', $data->ClientId) !!}
            {!! Form::hidden('JatuhTempo', \Carbon\Carbon::now()) !!}
        @else
            {{ Form::model($data, [
                'method' => 'PUT',
                'route' => ['transaksi.update', $data->TransaksiId]
            ]) }}
        @endif

        <div class="form-group">
            {!! Form::label('unitkos', 'Unit Kos', ['class' => 'control-label']) !!}
            {!! Form::select('UnitKosId', $UnitKos, null, ['placeholder' => 'Pilih Kamar..', 'class' => 'form-control', 'id' => 'UnitKos']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('tipe', 'Tipe Kos', ['class' => 'control-label']) !!}
            {!! Form::select('TipeKos', ['Bulanan' => 'Bulanan', 'Harian' => 'Harian'], null, ['placeholder' => 'Pilih Tipe..', 'class' => 'form-control', 'id' => 'TipeKos']) !!}
        </div>
        <div class="form-group">
                {!! Form::label('tanggal', 'Tanggal Kos (Bulan / Tanggal / Tahun)', ['class' => 'control-label']) !!}
                {!! Form::date('TanggalMasuk', (isset($data->TanggalMasuk))  ? \Carbon\Carbon::parse($data->TanggalMasuk)->format('Y-m-d') : \Carbon\Carbon::now(), ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('harga', 'Harga', ['class' => 'control-label']) !!}
            {!! Form::number('Harga', 0, ['class' => 'form-control', 'id' => 'Harga']) !!}
        </div>
        <div class="form-group">
            <div class="form-check">
                {!! Form::checkbox('Parkir', '1', isset($data->Parkir)?null:true, ['class' => 'form-check-input', 'id' => 'parkir', 'onclick' => 'checkboxParkir()']) !!}
                {!! Form::label('parkir', 'Parkir', ['class' => 'form-check-label', 'for' => 'parkir']) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="form-check">
                {!! Form::checkbox('Pembantu', '1', isset($data->Pembantu)?null:true, ['class' => 'form-check-input', 'id' => 'pembantu', 'onclick' => 'checkboxPembantu()']) !!}
                {!! Form::label('pembantu', 'Pembantu', ['class' => 'form-check-label', 'for' => 'pembantu']) !!}
            </div>
        </div>
        {{Form::submit('Submit',['class'=>'btn btn-success form-control'])}}
        {{ Form::close() }}
    </div>
</div>

@endsection

@push('scripts')
<script>
var TipeKos = document.getElementById('TipeKos'),
Parkir = document.getElementById('parkir'),
Pembantu = document.getElementById('pembantu'),
Harga = document.getElementById('Harga');

$(document).ready(function() {    
    $('input[type="checkbox"]').each(function(){
  	    $(this).prop('checked', false);
    });
    Harga.value = 0;
});


TipeKos.onchange = function() {
    var result = 0;
    if(TipeKos.value == 'Harian') {
        result = {{ $HargaHarian }};
        Parkir.checked = false;
        Pembantu.checked = false;
        Parkir.disabled = true;
        Pembantu.disabled = true;
    } else {
        result = {{ $HargaBulanan }}
        Parkir.disabled = false;
        Pembantu.disabled = false;
    }

    Harga.value = parseFloat(result);
};

function checkboxParkir() {
    if(Parkir.checked == true){
        Harga.value = parseFloat(Harga.value) + parseFloat({{ $Parkir }});
    } else {
        if(Harga.value >= parseFloat({{ $Parkir }})) {
            Harga.value = parseFloat(Harga.value) - parseFloat({{ $Parkir }});
        }
    }

}

function checkboxPembantu() {
    if(Pembantu.checked == true){
        Harga.value = parseFloat(Harga.value) + parseFloat({{ $Pembantu }});
    } else {
        if(Harga.value >= parseFloat({{ $Pembantu }})) {
            Harga.value = parseFloat(Harga.value) - parseFloat({{ $Pembantu }});
        }
    }

}
</script>
@endpush