@extends('layouts.dashboard')

@section('title')
Pengguna
@endsection

@section('content')
<div class="card mb-3">
  <div class="card-header">
    <i class="fa fa-table"></i> Data Pengguna</div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" width="100%" cellspacing="0">
          <tr>
              <td>Nama</td>
              <td>{{$data->ClientName}}</td>
          </tr>
          @if($data->Foto)
          <tr>
              <td>Foto</td>
                <td><a href="{{asset('storage/Foto/'.$data->Foto)}}" target="_blank"><img src="{{asset('storage/Foto/'.$data->Foto)}}" height="125" /></a></td>
          </tr>
          @endif
      </table>
    </div>
    @if(Session::has('message'))
      <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif
    <div class="float-right col-md-auto mb-2">
      <a href="{{route('transaksi.create', ['clientid' => $data->ClientId])}}">
        <button type="button" class="btn btn-primary float-right">Tambah Kamar Kost</button>
      </a>
    </div>
    <div class="float-left col-md-auto mb-2">
      <a href="{{route('users.edit', ['clientid' => $data->ClientId])}}">
          <button type="button" class="btn btn-success float-right">Ubah Info Pengguna</button>
      </a>
    </div>
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Unit Kos</th>
            <th>Tipe</th>
            <th>Tanggal Masuk</th>
            <th>Jatuh Tempo</th>
            <th>Tanggal Keluar</th>
            <th>Harga</th>
            <th>Parkir</th>
            <th>Pembantu</th>
            <th></th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="checkoutModal" tabindex="-1" role="dialog" aria-labelledby="checkout" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Checkout</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      {{ Form::open([
      'method' => 'PUT',
      'route' => ['transaksi.checkout',':id'], 
      'id' => 'checkoutForm'
      ]) }}
      <div class="modal-body">
        <div class="form-group">
          {!! Form::label('tanggalkeluar', 'Tanggal Keluar', ['class' => 'control-label']) !!}
          {!! Form::date('TanggalKeluar', \Carbon\Carbon::now(), ['class' => 'form-control']) !!}
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        {{Form::submit('Submit',['class'=>'btn btn-success form-control'])}}
      </div>
      {{ Form::close() }}

    </div>
  </div>
</div>
@endsection


@push('scripts')
<script src="{{asset('js/handlebars-v4.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {
  var template = Handlebars.compile($("#details-template").html());
  Handlebars.registerHelper("convertDate", function(input) {
    return convertDate(input);
  });
  Handlebars.registerHelper("convertCurrency", function(input) {
    return convertCurrency(input);
  });
  Handlebars.registerHelper('addPayment', function(input) {
    var link = '{{route("payment.create", ":id")}}';
    link = link.replace(':id', input);
    return new Handlebars.SafeString(
      "<a href='" + link + "'><button type='button' class='btn btn-primary'>Input Pembayaran</button></a>"
    );
  });
  Handlebars.registerHelper('deleteTransaksi', function(input) {
    var link = '{{route("transaksi.delete", ":id")}}';
    link = link.replace(':id', input);
    return new Handlebars.SafeString(
      '<a href="'+link+'" onclick="return confirm(\'Apakah anda yakin untuk menghapus seluruh transaksi?\')"><button type="button" id="delete" class="delete btn btn-danger">Hapus Transaksi</button></a>'
    );
  });
  Handlebars.registerHelper('deletePembayaran', function(input) {
    var link = '{{route("payment.delete", ":id")}}';
    link = link.replace(':id', input);
    return new Handlebars.SafeString(
      '<a href="'+link+'" onclick="return confirm(\'Apakah anda yakin untuk menghapus pembayaran ini?\')"><button type="button" id="delete" class="delete btn btn-danger">Hapus Pembayaran</button></a>'
    );
  });
  Handlebars.registerHelper('checkoutTransaksi', function(input) {
    return new Handlebars.SafeString(
      '<button type="button" id="checkouts" class="btn btn-warning" data-toggle="modal" data-target="#checkoutModal" data-val="'+input+'">Checkout Kost</button>'
    );
  });
  Handlebars.registerHelper('editTransaksi', function(input) {
    var link = '{{route("transaksi.edit", ":id")}}';
    link = link.replace(':id', input);
    return new Handlebars.SafeString(
      '<a href="'+link+'"><button type="button" id="edit" class="edit btn btn-success")">Ubah</button></a>'
    );
  });

  var ajaxUrl = '{{ route("transaksi.table",":id") }}';
  ajaxUrl =  ajaxUrl.replace(':id', '{{$data->ClientId}}');

    var table = $('#dataTable').DataTable( {
        "processing": true,
        "ajax": ajaxUrl,
        "columns": [ 
          { data: "unitkos", "render": function ( data, type, row, meta ) {
              return 'Lt. ' + data.Lantai + ' - ' + data.UnitKos;
            }
          },
          { data: "TipeKos" },
          { data: "TanggalMasuk", "render": function ( data, type, row, meta ) {
              return convertDate(data);
            }
          },
          { data: "JatuhTempo", "render": function ( data, type, row, meta ) {
              if (convertDate(data) == '31 Dec 2999') {
                return 'Selesai';
              } else {
                return convertDate(data);
              }
            }
          },
          { data: "TanggalKeluar", "render": function ( data, type, row, meta ) {
              return convertDate(data);
            }
          },
          { data: "Harga", "render": function ( data, type, row, meta ) {
              return convertCurrency(data);
            }
          },
          { data: "Parkir", "render": function ( data, type, row, meta ) {
              return booleanAnswer(data);
            }
          },
          { data: "Pembantu", "render": function ( data, type, row, meta ) {
              return booleanAnswer(data);
            }
          },
          { data: "", "render": function () {
                return '<button type="button" id="detail" class="detail btn btn-info">Detail</button>';
                }
          }
        ],
        "createdRow": function(row, data, dataIndex){
          if( new Date(data.JatuhTempo) <= new Date()){
            $(row).addClass('tableDuedate');
          }
        }
    } );

   // Add event listener for opening and closing details
   $('#dataTable tbody').on('click', 'button.detail', function () {
      var tr = $(this).closest('tr');
      var detail = tr.find('.detail');
      var row = table.row( tr );

      if ( row.child.isShown() ) {
          // This row is already open - close it
          row.child.hide();
          detail.addClass('btn-info');
          detail.removeClass('btn-dark');
      }
      else {
          // Open this row
          row.child( template(row.data()) ).show();
          detail.addClass('btn-dark');
          detail.removeClass('btn-info');
      }
  });

} );
</script>

<script>
//Modal URL
$(document).ready(function() {
  $('#checkoutModal').on('show.bs.modal', function (event) {
    var data = $(event.relatedTarget).data('val');
    var action = $('#checkoutForm').attr('action');
    action = action.replace(":id", data);
    $('#checkoutForm').attr('action', action);
  });
});
</script>

<script id="details-template" type="text/x-handlebars-template">
  @{{#each payment}}
    <table class="table">
        <tr>
            <td>Tanggal Pembayaran:</td>
            <td>@{{convertDate TanggalPembayaran}}</td>
            <td width="1%">@{{deletePembayaran PaymentId}}</td>
        </tr>
        <tr>
            <td>Jumlah:</td>
            <td colspan="2">@{{convertCurrency Jumlah}}</td>
        </tr>
    </table>
    <hr style="border-top: 3px double #8c8b8b;">
  @{{/each}}
  <div class="float-left">
    @{{{deleteTransaksi TransaksiId}}}
  </div>
  <div class="float-right">
    <!-- @{{{editTransaksi TransaksiId}}} -->
    @{{#if TanggalKeluar}}
    @{{else}}
      @{{{checkoutTransaksi TransaksiId}}}
    @{{/if}}
    @{{{addPayment TransaksiId}}}
  </div>
</script>
@endpush