@extends('layouts.dashboard')

@section('title')
Pengguna
@endsection

@section('content')
<div class="card mb-3">
    <div class="card-header">
        <i class="fa fa-table"></i> Payment
    </div>
    <div class="card-body">
        @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>Nama</th>
                    <th>Unit Kos</th>
                    <th>Tanggal Pembayaran</th>
                    <th>Jumlah</th>
                </tr>
                </thead>
            </table>
        </div>

    </div>
</div>
<div class="card mb-3">
    <div class="card-header">
        <i class="fa fa-table"></i> Monthly Payment
    </div>
    <div class="card-body">
        <div id="chart_div" style="width: 100%; min-height: 450px"></div>
    </div>
</div>
@endsection


@push('scripts')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">

// Load the Visualization API and the corechart package.
google.charts.load('current', {'packages':['corechart']});

// Set a callback to run when the Google Visualization API is loaded.
google.charts.setOnLoadCallback(drawChart);

// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function drawChart() {
  // Create the data table.
  var data = new google.visualization.DataTable();
  var rows = {!!$rows!!};
  data.addColumn('string', 'Months');
  data.addColumn('number', 'Total Payments');
  data.addRows(rows);

  // Set chart options
  var options = {
        curveType: 'function',
        legend: { position: 'bottom' }
    };

  // Instantiate and draw our chart, passing in some options.
  var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
  chart.draw(data, options);
}
</script>

<script type="text/javascript">
$(document).ready(function() {
    $('#dataTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "{{route('analytics.payment.table')}}",
        "columns": [ 
          { data: "transaksi.client.ClientName" },
          { data: "transaksi.unitkos", "render": function ( data, type, row, meta ) {
            return 'Lt. ' + data.Lantai + ' - ' + data.UnitKos;
            }
          },
          { data: "TanggalPembayaran", "render": function ( data, type, row, meta ) {
              return convertDate(data);
            }
          },
          { data: "Jumlah", "render": function ( data, type, row, meta ) {
              return convertCurrency(data);
            }
          }
        ]
    } );
} );
</script>

@endpush