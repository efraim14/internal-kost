@extends('layouts.dashboard')

@section('title')
Home
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="card text-white bg-primary m-3 col-md">
            <div class="card-header">Jumlah Kost Aktif</div>
            <div class="card-body">
                <h5 class="card-title">{{$ActiveUsers}}</h5>
            </div>
        </div>
        <div class="card text-white bg-danger m-3 col-md">
            <div class="card-header">Kost yang Jatuh Tempo</div>
            <div class="card-body">
                <h5 class="card-title">{{$DuedateUsers}}</h5>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="card text-white bg-warning m-3 col-md">
            <div class="card-header">Parkir yang aktif</div>
            <div class="card-body">
                <h5 class="card-title">{{$ActiveParkir}}</h5>
            </div>
        </div>
        <div class="card text-white bg-success m-3 col-md">
            <div class="card-header">Pembantu yang aktif</div>
            <div class="card-body">
                <h5 class="card-title">{{$ActivePembantu}}</h5>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md text-center">
            <div class="card m-3">
                <div class="card-header">Denah Kost</div>
                <div class="card-body">
                    @foreach($UnitKos as $Lantai => $Kosts)
                        <div class="row mx-auto">
                        Lantai {{$Lantai}} - 
                        @foreach($Kosts as $Kost)
                            @if($Kost->IsAvailable == 1)
                                <div class="roomBox isActive m-1 px-2">
                                    {{$Kost->UnitKos}}
                                </div>
                            @else
                                <div class="roomBox isInactive m-1 px-2">
                                    {{$Kost->UnitKos}}
                                </div>
                            @endif
                        @endforeach
                        </div>
                    @endforeach

                    <div class="row"><div class="roomBox isActive m-1 p-2"></div>Unit Kost Tersedia</div>
                    <div class="row"><div class="roomBox isInactive m-1 p-2"></div> Unit Kost Terisi / Tidak Tersedia</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection