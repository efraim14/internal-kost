<!DOCTYPE html>
<html lang="en" style="position: relative;
  min-height: 100%;"><body class="fixed-nav sticky-footer bg-dark" id="page-top" style="overflow-x:hidden;margin-bottom:56px;">

  <!-- Sidebar -->
  <div class="content-wrapper" style="min-height:calc(100vh - 56px - 56px);overflow-x:hidden;background:white">
    <div class="container-fluid">
        <table class="table" style="width:100%;max-width:100%;margin-bottom:1rem;background-color:transparent;border-collapse:collapse!important;">
        @foreach($data as $client)
            <tr style="page-break-inside:avoid">
<td colspan="2" class="text-center table-secondary" style="padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6;text-align:center;background-color: lightgray;"><h4 style="margin-top:0;margin-bottom:.5rem;font-family:inherit;font-weight:500;line-height:1.2;color:inherit;font-size:1.5rem;">{{$client->ClientName}}</h4></td>
            </tr>
            @foreach($client->transaksi as $transaksi)
                <tr style="page-break-inside:avoid">
<td style="padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6;background-color:#fff!important;">Unit Kos</td> <td style="padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6;background-color:#fff!important;">Lantai {{$transaksi->unitkos->Lantai}} - Unit {{$transaksi->unitkos->UnitKosId}}</td>
                </tr>
<tr style="page-break-inside:avoid">
<td style="padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6;background-color:#fff!important;">Harga</td> <td style="padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6;background-color:#fff!important;">{{number_format($transaksi->Harga)}}</td>
                </tr>
<tr class="mb-2" style="page-break-inside:avoid">
<td style="padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6;background-color:#fff!important;">Tanggal Jatuh Tempo</td> <td style="padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6;background-color:#fff!important;">{{date("d-M-Y", strtotime($transaksi->JatuhTempo))}}</td>
                </tr>
                </tr>
<tr style="page-break-inside:avoid">
<td style="padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6;background-color:#fff!important;">Parkir</td> <td style="padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6;background-color:#fff!important;">{!! $transaksi->Parkir == 1 ? 'Ya' : 'Tidak' !!}</td>
                </tr>
                </tr>
<tr style="page-break-inside:avoid">
<td style="padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6;background-color:#fff!important;">Pembantu</td> <td style="padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6;background-color:#fff!important;">{!! $transaksi->Pembantu == 1 ? 'Ya' : 'Tidak' !!}</td>
                </tr>
            @endforeach
        @endforeach
        </table>
</div>
 
    <footer class="sticky-footer" style="width:100%;display:block;position:absolute;right:0;bottom:0;height:56px;background-color:#e9ecef;line-height:55px;text-align:center"><div class="container">
        <div class="text-center">
          <small style="font-size:80%;font-weight:400;">Copyright &copy; Y 2018</small>
        </div>
      </div>
    </footer>
</div>
 
</body></html>