<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
	<!--<![endif]-->
    
    <style type="text/css" id="media-query">
      body {
  margin: 0;
  padding: 0; }

table, tr, td {
  vertical-align: top;
  border-collapse: collapse; }

.ie-browser table, .mso-container table {
  table-layout: fixed; }

* {
  line-height: inherit; }

a[x-apple-data-detectors=true] {
  color: inherit !important;
  text-decoration: none !important; }

[owa] .img-container div, [owa] .img-container button {
  display: block !important; }

[owa] .fullwidth button {
  width: 100% !important; }

[owa] .block-grid .col {
  display: table-cell;
  float: none !important;
  vertical-align: top; }

.ie-browser .num12, .ie-browser .block-grid, [owa] .num12, [owa] .block-grid {
  width: 620px !important; }

.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
  line-height: 100%; }

.ie-browser .mixed-two-up .num4, [owa] .mixed-two-up .num4 {
  width: 204px !important; }

.ie-browser .mixed-two-up .num8, [owa] .mixed-two-up .num8 {
  width: 408px !important; }

.ie-browser .block-grid.two-up .col, [owa] .block-grid.two-up .col {
  width: 310px !important; }

.ie-browser .block-grid.three-up .col, [owa] .block-grid.three-up .col {
  width: 206px !important; }

.ie-browser .block-grid.four-up .col, [owa] .block-grid.four-up .col {
  width: 155px !important; }

.ie-browser .block-grid.five-up .col, [owa] .block-grid.five-up .col {
  width: 124px !important; }

.ie-browser .block-grid.six-up .col, [owa] .block-grid.six-up .col {
  width: 103px !important; }

.ie-browser .block-grid.seven-up .col, [owa] .block-grid.seven-up .col {
  width: 88px !important; }

.ie-browser .block-grid.eight-up .col, [owa] .block-grid.eight-up .col {
  width: 77px !important; }

.ie-browser .block-grid.nine-up .col, [owa] .block-grid.nine-up .col {
  width: 68px !important; }

.ie-browser .block-grid.ten-up .col, [owa] .block-grid.ten-up .col {
  width: 62px !important; }

.ie-browser .block-grid.eleven-up .col, [owa] .block-grid.eleven-up .col {
  width: 56px !important; }

.ie-browser .block-grid.twelve-up .col, [owa] .block-grid.twelve-up .col {
  width: 51px !important; }

@media only screen and (min-width: 640px) {
  .block-grid {
    width: 620px !important; }
  .block-grid .col {
    vertical-align: top; }
    .block-grid .col.num12 {
      width: 620px !important; }
  .block-grid.mixed-two-up .col.num4 {
    width: 204px !important; }
  .block-grid.mixed-two-up .col.num8 {
    width: 408px !important; }
  .block-grid.two-up .col {
    width: 310px !important; }
  .block-grid.three-up .col {
    width: 206px !important; }
  .block-grid.four-up .col {
    width: 155px !important; }
  .block-grid.five-up .col {
    width: 124px !important; }
  .block-grid.six-up .col {
    width: 103px !important; }
  .block-grid.seven-up .col {
    width: 88px !important; }
  .block-grid.eight-up .col {
    width: 77px !important; }
  .block-grid.nine-up .col {
    width: 68px !important; }
  .block-grid.ten-up .col {
    width: 62px !important; }
  .block-grid.eleven-up .col {
    width: 56px !important; }
  .block-grid.twelve-up .col {
    width: 51px !important; } }

@media (max-width: 640px) {
  .block-grid, .col {
    min-width: 320px !important;
    max-width: 100% !important;
    display: block !important; }
  .block-grid {
    width: calc(100% - 40px) !important; }
  .col {
    width: 100% !important; }
    .col > div {
      margin: 0 auto; }
  img.fullwidth, img.fullwidthOnMobile {
    max-width: 100% !important; }
  .no-stack .col {
    min-width: 0 !important;
    display: table-cell !important; }
  .no-stack.two-up .col {
    width: 50% !important; }
  .no-stack.mixed-two-up .col.num4 {
    width: 33% !important; }
  .no-stack.mixed-two-up .col.num8 {
    width: 66% !important; }
  .no-stack.three-up .col.num4 {
    width: 33% !important; }
  .no-stack.four-up .col.num3 {
    width: 25% !important; }
  .mobile_hide {
    min-height: 0px;
    max-height: 0px;
    max-width: 0px;
    display: none;
    overflow: hidden;
    font-size: 0px; } }

    </style>
</head>
<body class="clean-body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #FFFFFF">
  <style type="text/css" id="media-query-bodytag">
    @media (max-width: 520px) {
      .block-grid {
        min-width: 320px!important;
        max-width: 100%!important;
        width: 100%!important;
        display: block!important;
      }

      .col {
        min-width: 320px!important;
        max-width: 100%!important;
        width: 100%!important;
        display: block!important;
      }

        .col > div {
          margin: 0 auto;
        }

      img.fullwidth {
        max-width: 100%!important;
      }
			img.fullwidthOnMobile {
        max-width: 100%!important;
      }
      .no-stack .col {
				min-width: 0!important;
				display: table-cell!important;
			}
			.no-stack.two-up .col {
				width: 50%!important;
			}
			.no-stack.mixed-two-up .col.num4 {
				width: 33%!important;
			}
			.no-stack.mixed-two-up .col.num8 {
				width: 66%!important;
			}
			.no-stack.three-up .col.num4 {
				width: 33%!important;
			}
			.no-stack.four-up .col.num3 {
				width: 25%!important;
			}
      .mobile_hide {
        min-height: 0px!important;
        max-height: 0px!important;
        max-width: 0px!important;
        display: none!important;
        overflow: hidden!important;
        font-size: 0px!important;
      }
    }
  </style>

<div style="background-color:transparent;">
    <div style="Margin: 0 auto;min-width: 320px;max-width: 620px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #000000;"
        class="block-grid mixed-two-up ">
        <div style="border-collapse: collapse;display: table;width: 100%;background-color:#000000;">
        <div class="col" style="display: table-cell;vertical-align: top;min-width: 320px;max-width: 408px;">
            <div style="background-color: transparent; width: 100% !important;">
            <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                <div class="">
                <div style="color:#FFFFFF;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:120%; padding-right: 20px; padding-left: 20px; padding-top: 10px; padding-bottom: 10px;">
                    <div style="font-size:12px;line-height:14px;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;color:#FFFFFF;text-align:center;">
                    <p style="margin: 0;font-size: 14px;line-height: 17px">Denah Kost tanggal {{\Carbon\Carbon::now()->format('d-m-Y')}}</p>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>

@foreach($UnitKos as $Lantai => $Kosts)
<div style="background-color:transparent;">
    <div style="Margin: 0 auto;min-width: 320px;max-width: 620px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;"
        class="block-grid mixed-two-up ">
        <div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
            <div class="col" style="display: table-cell;vertical-align: top;min-width: 320px;max-width: 408px;">
                <div style="background-color: transparent; width: 100% !important;">
                    <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:15px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                        <div class="">
                            <div style="color:#000000;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:120%; padding-right: 20px; padding-left: 20px">
                                <div style="font-size:12px;line-height:14px;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;color:#000000;text-align:center;display:table;margin: 0 auto">
                                    <p style="margin: 0;font-size: 14px;line-height: 17px;display: inline-block; ">
                                        <span style="color: rgb(0, 0, 0); font-size: 14px; line-height: 16px;">
                                            Lantai {{$Lantai}} - 
                                            @foreach($Kosts as $Kost)
                                                @if($Kost->IsAvailable == 1)
                                                    <div style="display: inline-block;border-radius: 5px;border: 1px solid black;background-color: #90EE90;padding-left: .5rem;padding-right: .5rem;margin: .25rem">
                                                        {{$Kost->UnitKos}}
                                                    </div>
                                                @else
                                                    <div style="display: inline-block;border-radius: 5px;border: 1px solid black;background-color: #FFC0CB;padding-left: .5rem;padding-right: .5rem;margin: .25rem">
                                                        {{$Kost->UnitKos}}
                                                    </div>
                                                @endif
                                            @endforeach
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
<div style="background-color:transparent;">
    <div style="Margin: 0 auto;min-width: 320px;max-width: 620px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;"
        class="block-grid ">
        <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
        <div class="col num12" style="min-width: 320px;max-width: 620px;display: table-cell;vertical-align: top;">
            <div style="background-color: transparent; width: 100% !important;">
            <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:25px; padding-right: 0px; padding-left: 0px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="divider " style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                <tbody>
                    <tr style="vertical-align: top">
                    <td class="divider_inner" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-right: 10px;padding-left: 10px;padding-top: 15px;padding-bottom: 10px;min-width: 100%;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                        <table class="divider_content" align="center" border="0" cellpadding="0" cellspacing="0"
                        width="100%" style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 1px dotted #CCCCCC;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                        <tbody>
                            <tr style="vertical-align: top">
                            <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                <span></span>
                            </td>
                            </tr>
                        </tbody>
                        </table>
                    </td>
                    </tr>
                </tbody>
                </table>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>

<div style="background-color:transparent;">
    <div style="Margin: 0 auto;min-width: 320px;max-width: 620px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #000000;"
        class="block-grid mixed-two-up ">
        <div style="border-collapse: collapse;display: table;width: 100%;background-color:#000000;">
        <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 620px;"><tr class="layout-full-width" style="background-color:#000000;"><![endif]-->

        <!--[if (mso)|(IE)]><td align="center" width="413" style=" width:413px; padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
        <div class="col num8" style="display: table-cell;vertical-align: top;min-width: 320px;max-width: 408px;">
            <div style="background-color: transparent; width: 100% !important;">
            <!--[if (!mso)&(!IE)]><!-->
            <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                <!--<![endif]-->


                <div class="">
                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 20px; padding-left: 20px; padding-top: 10px; padding-bottom: 10px;"><![endif]-->
                <div style="color:#FFFFFF;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:120%; padding-right: 20px; padding-left: 20px; padding-top: 10px; padding-bottom: 10px;">
                    <div style="font-size:12px;line-height:14px;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;color:#FFFFFF;text-align:left;">
                    <p style="margin: 0;font-size: 14px;line-height: 17px">KATEGORI</p>
                    </div>
                </div>
                <!--[if mso]></td></tr></table><![endif]-->
                </div>

                <!--[if (!mso)&(!IE)]><!-->
            </div>
            <!--<![endif]-->
            </div>
        </div>
        <!--[if (mso)|(IE)]></td><td align="center" width="207" style=" width:207px; padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
        <div class="col num4" style="display: table-cell;vertical-align: top;max-width: 320px;min-width: 204px;">
            <div style="background-color: transparent; width: 100% !important;">
            <!--[if (!mso)&(!IE)]><!-->
            <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                <!--<![endif]-->


                <div class="">
                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 20px; padding-left: 20px; padding-top: 10px; padding-bottom: 10px;"><![endif]-->
                <div style="color:#FFFFFF;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:120%; padding-right: 20px; padding-left: 20px; padding-top: 10px; padding-bottom: 10px;">
                    <div style="font-size:12px;line-height:14px;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;color:#FFFFFF;text-align:left;">
                    <p style="margin: 0;font-size: 14px;line-height: 17px"><span style="font-size: 14px; line-height: 16px;">JUMLAH</span><br></p>
                    </div>
                </div>
                <!--[if mso]></td></tr></table><![endif]-->
                </div>

                <!--[if (!mso)&(!IE)]><!-->
            </div>
            <!--<![endif]-->
            </div>
        </div>
        <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
        </div>
    </div>
</div>
<div style="background-color:transparent;">
    <div style="Margin: 0 auto;min-width: 320px;max-width: 620px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;"
        class="block-grid mixed-two-up ">
        <div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
        <div class="col" style="display: table-cell;vertical-align: top;min-width: 320px;max-width: 408px;">
            <div style="background-color: transparent; width: 100% !important;">
            <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:15px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                <div class="">
                <div style="color:#000000;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:120%; padding-right: 20px; padding-left: 20px; padding-top: 10px; padding-bottom: 10px;">
                    <div style="font-size:12px;line-height:14px;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;color:#000000;text-align:left;">
                    <p style="margin: 0;font-size: 14px;line-height: 17px"><span style="color: rgb(0, 0, 0); font-size: 14px; line-height: 16px;">Jumlah Kost yang menyewa</span></p>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        <div class="col num4" style="display: table-cell;vertical-align: top;max-width: 320px;min-width: 204px;">
            <div style="background-color: transparent; width: 100% !important;">
            <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:15px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                <div class="">
                <div style="color:#000000;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:120%; padding-right: 20px; padding-left: 20px; padding-top: 10px; padding-bottom: 10px;">
                    <div style="font-size:12px;line-height:14px;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;color:#000000;text-align:left;">
                    <p style="margin: 0;font-size: 14px;line-height: 17px">{{$MonthlyPayment[0]->Payment}}</p>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
<div style="background-color:transparent;">
    <div style="Margin: 0 auto;min-width: 320px;max-width: 620px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;"
        class="block-grid mixed-two-up ">
        <div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
        <div class="col" style="display: table-cell;vertical-align: top;min-width: 320px;max-width: 408px;">
            <div style="background-color: transparent; width: 100% !important;">
            <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:15px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                <div class="">
                <div style="color:#000000;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:120%; padding-right: 20px; padding-left: 20px; padding-top: 10px; padding-bottom: 10px;">
                    <div style="font-size:12px;line-height:14px;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;color:#000000;text-align:left;">
                    <p style="margin: 0;font-size: 14px;line-height: 17px"><span style="color: rgb(0, 0, 0); font-size: 14px; line-height: 16px;">Jumlah yang sudah dibayarkan</span></p>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        <div class="col num4" style="display: table-cell;vertical-align: top;max-width: 320px;min-width: 204px;">
            <div style="background-color: transparent; width: 100% !important;">
            <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:15px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                <div class="">
                <div style="color:#000000;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:120%; padding-right: 20px; padding-left: 20px; padding-top: 10px; padding-bottom: 10px;">
                    <div style="font-size:12px;line-height:14px;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;color:#000000;text-align:left;">
                    <p style="margin: 0;font-size: 14px;line-height: 17px">{{$MonthlyTransaction[0]->Transaksi}}</p>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
</body>