<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/users/table', 'UsersController@table')->name('users.table');
Route::get('/users', 'UsersController@index')->name('users.index');
Route::get('/users/create', 'UsersController@create')->name('users.create');
Route::post('/users', 'UsersController@store')->name('users.store');
Route::get('/users/edit/{user}', 'UsersController@edit')->name('users.edit');
Route::put('/users/{user}', 'UsersController@update')->name('users.update');
Route::get('/users/delete/{clientid}', 'UsersController@destroy')->name('users.delete');

Route::get('/transaksi/{clientid}', 'TransactionController@index')->name('transaksi.index');
Route::get('/transaksi/table/{clientid}', 'TransactionController@table')->name('transaksi.table');
Route::get('/transaksi/create/{transaksi}', 'TransactionController@create')->name('transaksi.create');
Route::put('/transaksi/checkout/{transaksi}', 'TransactionController@checkout')->name('transaksi.checkout');
Route::post('/transaksi', 'TransactionController@store')->name('transaksi.store');
Route::get('/transaksi/edit/{transaksi}', 'TransactionController@edit')->name('transaksi.edit');
Route::put('/transaksi/{transaksi}', 'TransactionController@update')->name('transaksi.update');
Route::get('/transaksi/delete/{transaksi}', 'TransactionController@destroy')->name('transaksi.delete');

Route::get('/payment/create/{transaksiid}', 'PaymentController@create')->name('payment.create');
Route::get('/payment/delete/{paymentid}', 'PaymentController@destroy')->name('payment.delete');
Route::post('/payment', 'PaymentController@store')->name('payment.store');

Route::get('/analytics', 'AnalyticsController@index')->name('analytics.index');
Route::get('/analytics/payment/table', 'AnalyticsController@payment_table')->name('analytics.payment.table');

Route::get('/settings', 'SettingsController@index')->name('settings.index');
Route::put('/settings', 'SettingsController@update')->name('settings.update');

// Route::get('/test', 'HomeController@test');

// Route::get('/demo', function () {
//     return new \App\Mail\AccountantMail();
// });

// Route::view('/template', 'template');