<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table='payment';
    protected $primaryKey='PaymentId';
    protected $fillable = ['TransaksiId', 'TanggalPembayaran', 'UpdateJatuhTempo', 'Jumlah'];

    public function transaksi() {
        return $this->belongsTo('App\Transaksi', 'TransaksiId', 'TransaksiId');
    }
}
