<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $table='settings';
    protected $primaryKey='SettingsId';
    protected $fillable = ['Name', 'Label', 'Value'];

}
