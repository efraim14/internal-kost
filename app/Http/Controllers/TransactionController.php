<?php

namespace App\Http\Controllers;

use Session;
use App\Client;
use DataTables;
use App\Duedate;
use App\Payment;
use App\UnitKos;
use App\Settings;
use App\Transaksi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\DuedateController;

class TransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $data['data'] = Client::where('ClientId',$id)->first();
        return view('transaksi.index')->with($data);
    }

    
    public function table($id)
    {
        $query = Transaksi::with(['payment', 'unitkos'])->where('ClientId',$id)->get();
        return Datatables::of($query)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $data['data'] = Client::where('ClientId',$id)->first();
        $data['UnitKos'] = UnitKos::select(DB::raw("CONCAT('Lantai ', Lantai, ' - Unit ',UnitKos) AS Name"), 'UnitKosId')->where('IsAvailable', 1)->pluck('Name', 'UnitKosId');
        $data['HargaHarian'] = Settings::where('Name', 'HargaHarian')->first()->Value;
        $data['HargaBulanan'] = Settings::where('Name', 'HargaBulanan')->first()->Value;
        $data['Parkir'] = Settings::where('Name', 'Parkir')->first()->Value;
        $data['Pembantu'] = Settings::where('Name', 'Pembantu')->first()->Value;
        $data['method'] = 'create';
        return view('transaksi.create')->with($data);
    }

    public function checkout(Request $request, $id)
    {
        $transaksi = Transaksi::findOrFail($id);

        // $client->ClientName = $request->ClientName;
        $this->validate($request, [
            'TanggalKeluar' => 'required|date'
            ]);
        $data = $request->all();
        $transaksi->fill($data)->save();

        $UnitKos = UnitKos::findOrFail($transaksi->unitkos->UnitKosId);
        $UnitKos->IsAvailable = 1;
        $UnitKos->save();

        Session::flash('message', 'Transaksi dengan Lantai '. $transaksi->unitkos->Lantai .' - Unit '. $transaksi->unitkos->UnitKos .' telah berhasil di checkout'); 
        Session::flash('alert-class', 'alert-info');
        return redirect()->route('transaksi.index', ['clientid' => $transaksi->ClientId]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'TipeKos' => 'required',
            'TanggalMasuk' => 'required|date',
            'Harga' => 'required|numeric'
            ]);

        $Transaksi = Transaksi::create($request->all());
        
        $UnitKos = UnitKos::findOrFail($request->UnitKosId);
        $UnitKos->IsAvailable = 0;
        $UnitKos->save();

        Session::flash('message', 'Transaksi telah berhasil ditambahkan'); 
        Session::flash('alert-class', 'alert-info');
        return redirect()->route('transaksi.index', ['clientid' => $request->ClientId]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = array();
        $data['data'] =  Transaksi::findOrFail($id);
        $data['method'] = 'edit';
        return view('transaksi.create')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transaksi = Transaksi::findOrFail($id);

        // $client->ClientName = $request->ClientName;
        $this->validate($request, [
            'TipeKos' => 'required',
            'TanggalMasuk' => 'required|date',
            'Harga' => 'required|numeric'
            ]);
        $data = $request->all();
        if(!isset($data['Parkir'])) $data['Parkir'] = 0;
        if(!isset($data['Pembantu'])) $data['Pembantu'] = 0;

        $transaksi->fill($data)->save();

        Session::flash('message', 'Informasi Pengguna telah berhasil diubah'); 
        Session::flash('alert-class', 'alert-info');
        return redirect()->route('transaksi.index', ['clientid' => $transaksi->ClientId]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Transaksi = Transaksi::with('client')->where('TransaksiId',$id)->first();

        Transaksi::where('TransaksiId',$id)->delete();
        Payment::where('TransaksiId',$id)->delete();

        if($Transaksi->TanggalKeluar == null) {
            $UnitKos = UnitKos::findOrFail($Transaksi->unitkos->UnitKosId);
            $UnitKos->IsAvailable = 1;
            $UnitKos->save();
        }

        Session::flash('message', 'Transaksi ' . $Transaksi->client->ClientName . ' dengan Lantai '. $Transaksi->unitkos->Lantai .' - Unit '. $Transaksi->unitkos->UnitKos .' telah berhasil dihapus'); 
        Session::flash('alert-class', 'alert-danger');
        return redirect()->route('transaksi.index', ['clientid' => $Transaksi->client->ClientId]);
    }
}
