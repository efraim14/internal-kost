<?php

namespace App\Http\Controllers;

use DataTables;
use App\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AnalyticsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Payment = Payment::where('TanggalPembayaran', '>', Carbon::now()->subMonths(12))
                    ->selectRaw('DATE_FORMAT(TanggalPembayaran, "%M %Y") as Tanggal, sum(Jumlah) as Payment')
                    ->orderBy('TanggalPembayaran')
                    ->groupBy(\DB::raw('DATE_FORMAT(TanggalPembayaran, "%M %Y")'))
                    ->get();
        $data['rows'] = $Payment->map(function($i) {
                            return array_values($i->toArray());
                        });
        return view('analytics.index')->with($data);
    }

    public function payment_table() 
    {
        
        return Datatables::of(Payment::with(['transaksi','transaksi.client', 'transaksi.unitkos'])->latest()->get())->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
