<?php

namespace App\Http\Controllers;

use App\Client;
use App\Duedate;
use App\Payment;
use App\UnitKos;
use App\Transaksi;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['ActiveUsers'] = Client::whereHas('transaksi', function ($query) {
            $query->whereNull('TanggalKeluar');
        })->count();

        $data['DuedateUsers'] = Client::whereHas('transaksi', function ($query) {
            $query->where('JatuhTempo', '<=', Carbon::now()->toDateString());
        })->count();

        $data['ActiveParkir'] = Client::whereHas('transaksi', function ($query) {
            $query->whereNull('TanggalKeluar')->where('Parkir', 1);
        })->count();

        $data['ActivePembantu'] = Client::whereHas('transaksi', function ($query) {
            $query->whereNull('TanggalKeluar')->where('Pembantu', 1);
        })->count();

        $data['UnitKos'] = array();
        $UnitKos = UnitKos::get();
        //Group By Lantai
        foreach ($UnitKos as $element) {
            $data['UnitKos'][$element['Lantai']][] = $element;
        }
        
        return view('home')->with($data);
    }

    public function test()
    {
        $data['MonthlyPayment'] = Payment::whereMonth('TanggalPembayaran', '=', Carbon::now()->month)
                                ->selectRaw('sum(Jumlah) as Payment')
                                ->get();

        $data['MonthlyTransaction'] = Transaksi::whereNull('TanggalKeluar')
                                    ->selectRaw('sum(Harga) as Transaksi')
                                    ->get();


        // $data['data'] = Client::whereHas('transaksi.duedate', function($query) {
        //     $query->latest('duedate.Tanggal')->where('duedate.Tanggal', '<=', Carbon::now()->toDateString())->limit(1);
        // })->get();

        return $data;
        ;
    }
}
