<?php

namespace App\Http\Controllers;

use Session;
use App\Client;
use DataTables;
use App\Duedate;
use App\Payment;
use App\UnitKos;
use App\Transaksi;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index');
    }

    public function table() 
    {
        
        return Datatables::of(Client::with(['transaksi' => function ($query) {
            $query->whereNull('TanggalKeluar');
        }])->get())->make(true);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['method'] = 'create';
        return view('users.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'ClientName' => 'required|max:190',
            'Foto' => 'mimes:jpeg,bmp,png,gif|max:10240'
            ]);
        $data = $request->all();

        if($request->Foto){
            $data['Foto'] = str_random(20).'-'.$request->ClientName.'-'.$request->Foto->getClientOriginalName();
            
            $request->file('Foto')->storeAs(
                'public/Foto', $data['Foto']
            );
        }
        
        Client::create($data);

        Session::flash('message', $request->ClientName.' telah berhasil ditambahkan'); 
        Session::flash('alert-class', 'alert-info');
        return redirect()->route('users.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = array();
        $data['data'] =  Client::findOrFail($id);
        $data['method'] = 'edit';
        return view('users.create')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client = Client::findOrFail($id);

        // $client->ClientName = $request->ClientName;
        $this->validate($request, [
            'ClientName' => 'required|max:190',
            'Foto' => 'mimes:jpeg,bmp,png,gif|max:10240'
            ]);
        $data = $request->all();

        if($request->Foto){
            $data['Foto'] = str_random(20).'-'.$request->ClientName.'-'.$request->Foto->getClientOriginalName();
            
            $request->file('Foto')->storeAs(
                'public/Foto', $data['Foto']
            );
        }

        $client->fill($data)->save();

        Session::flash('message', 'Informasi Pengguna telah berhasil diubah'); 
        Session::flash('alert-class', 'alert-info');
        return redirect()->route('transaksi.index', ['clientid' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Client = Client::where('ClientId',$id)->first();

        Client::where('ClientId',$id)->delete();
        $TransaksiId = Transaksi::select('TransaksiId')->where('ClientId',$id)->get();
        $UnitKosId = Transaksi::select('UnitKosId')->where('ClientId',$id)->whereNull('TanggalKeluar')->get();

        Transaksi::where('ClientId',$id)->delete();
        Payment::whereIn('TransaksiId',$TransaksiId)->delete();
        UnitKos::whereIn('UnitKosId',$UnitKosId)->update(['IsAvailable' => 1]);

        Session::flash('message', $Client->ClientName.' telah berhasil dihapus'); 
        Session::flash('alert-class', 'alert-danger');
        return redirect()->route('users.index');
    }
}
