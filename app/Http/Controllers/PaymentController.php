<?php

namespace App\Http\Controllers;

use Session;
use App\Payment;
use App\Settings;
use App\Transaksi;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $data['data'] = Transaksi::where('TransaksiId',$id)->first();
        $data['method'] = 'create';
        $data['JatuhTempo'] = Settings::where('Name', 'JatuhTempo')->first()->Value;
        return view('payment.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'TanggalPembayaran' => 'required',
            'UpdateJatuhTempo' => 'required',
            'Jumlah' => 'required|numeric'
            ]);

        $Payment = Payment::create($request->all());

        $Transaksi = Transaksi::findOrFail($Payment->TransaksiId);
        $Transaksi->JatuhTempo = $request->UpdateJatuhTempo;
        $Transaksi->save();
        
        Session::flash('message', 'Pembayaran telah berhasil ditambahkan'); 
        Session::flash('alert-class', 'alert-info');
        return redirect()->route('transaksi.index', ['clientid' => $request->ClientId]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Payment = Payment::with('transaksi')->where('PaymentId',$id)->first();
        $Transaksi = Transaksi::findOrFail($Payment->TransaksiId);

        Payment::where('PaymentId',$id)->delete();

        $PaymentUpdate = Payment::with('transaksi')->where('TransaksiId',$Payment->TransaksiId)->latest()->first();
        
        if($PaymentUpdate) {
            $Transaksi->JatuhTempo = $PaymentUpdate->UpdateJatuhTempo;
        } else {
            $Transaksi->JatuhTempo = $Transaksi->TanggalMasuk;
        }
        $Transaksi->save();
        
        Session::flash('message', 'Pembayaran pada tanggal ' . $Payment->TanggalPembayaran . ' sejumlah Rp. ' . $Payment->Jumlah . ' telah berhasil dihapus'); 
        Session::flash('alert-class', 'alert-danger');
        return redirect()->route('transaksi.index', ['clientid' => $Payment->transaksi->ClientId]);
    }
}
