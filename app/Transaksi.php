<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table='transaksi';
    protected $primaryKey='TransaksiId';
    protected $fillable = ['ClientId', 'UnitKosId', 'TipeKos', 'TanggalMasuk', 'TanggalKeluar', 'JatuhTempo', 'Harga', 'Parkir', 'Pembantu'];

    public function client() {
        return $this->belongsTo('App\Client', 'ClientId', 'ClientId');
    }

    public function payment() {
        return $this->hasMany('App\Payment', 'TransaksiId', 'TransaksiId');
    }

    public function unitkos() {
        return $this->belongsTo('App\UnitKos', 'UnitKosId', 'UnitKosId');
    }
}
