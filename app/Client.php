<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table='client';
    protected $primaryKey='ClientId';
    protected $fillable = ['ClientName', 'Foto'];

    public function transaksi() {
        return $this->hasMany('App\Transaksi', 'ClientId', 'ClientId');
    }
    
}
