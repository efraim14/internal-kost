<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Duedate extends Model
{
    protected $table='duedate';
    protected $primaryKey='DuedateId';
    protected $fillable = ['TransaksiId', 'Tanggal'];

    public function transaksi() {
        return $this->belongsTo('App\Transaksi', 'TransaksiId', 'TransaksiId');
    }
}
