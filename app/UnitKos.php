<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitKos extends Model
{
    protected $table='unitkos';
    protected $primaryKey='UnitKosId';
    protected $fillable = ['UnitKos','Lantai','IsAvailable'];

    public function transaksi() {
        return $this->hasMany('App\Transaksi', 'UnitKosId', 'UnitKosId');
    }
}
