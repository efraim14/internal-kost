<?php

namespace App\Console\Commands;

use App\Settings;
use App\Mail\DuedateMail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:duedate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email for user information';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $settings = Settings::where('Name','RecipientMail')->first();
        
        Mail::to($settings->Value)->send(new DuedateMail());
    }
}
