<?php

namespace App\Mail;

use App\Client;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DuedateMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['data'] = Client::with(['transaksi' => function ($query) {
            $query->where('JatuhTempo', '<=', Carbon::now()->addDays(2)->toDateString())->whereNull('TanggalKeluar');
        }])->whereHas('transaksi', function ($query) {
            $query->where('JatuhTempo', '<=', Carbon::now()->addDays(2)->toDateString())->whereNull('TanggalKeluar');
        })->get();

        return $this->subject('[DAILY] Jatuh Tempo User '.Carbon::now()->format('d/m/Y'))
        ->view('mail.duedate')->with($data);
    }
}
