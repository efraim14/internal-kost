<?php

namespace App\Mail;

use App\Payment;
use App\UnitKos;
use App\Transaksi;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AccountantMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['UnitKos'] = array();
        $UnitKos = UnitKos::with(['transaksi' => function ($query) {
            $query->whereNull('TanggalKeluar');
        }])->get();
        //Group By Lantai
        foreach ($UnitKos as $element) {
            $data['UnitKos'][$element['Lantai']][] = $element;
        }

        $data['MonthlyPayment'] = Payment::whereMonth('TanggalPembayaran', '=', Carbon::now()->month)
                                    ->selectRaw('sum(Jumlah) as Payment')
                                    ->get();

        $data['MonthlyTransaction'] = Transaksi::whereNull('TanggalKeluar')
                                    ->selectRaw('sum(Harga) as Transaksi')
                                    ->get();


        return $this->subject('***[MONTHLY]*** Laporan Bulanan '.Carbon::now()->format('d/m/Y'))
        ->view('mail.accountant')->with($data);
    }
}
